# Alpine Packer

This is a Packer template for building Alpine Linux in OpenStack with GitLab CI.

It uses packer-qemu to build the initial image, install cloud-init and [haveged](https://wiki.alpinelinux.org/wiki/Entropy_and_randomness#Haveged).

This template is designed to run from GitLab-CI with appropriate `OS_*` environment variables set.  It can be run manually:

```
bash script/gen-vars-json.sh
bash script/packer_build.sh
```

This results in a file that can be sourced:

```
. ./scrip/env-variables.sh
```

And the template in `qemu-output` directory.
