#!/usr/bin/env sh
set -ux

echo "[!] Dump disk info before doing anything"
df -h

sed -e '/cdrom/ s/^#*/#/' -i /etc/apk/repositories

echo "upgrade and insatll cloud-init"
apk upgrade --update-cache --available

echo "[!] Dump disk info after upgrade"
df -h

apk add cloud-init cloud-init-openrc haveged acpi sudo cloud-utils eudev
echo "[!] Dump disk info after installing packages"
df -h

echo "start services on boot"
rc-update add cloud-init default
rc-update add sshd default

cloud_random_config=/etc/cloud/cloud.cfg.d/96-random.cfg
echo "[!] Writing cloud_random config"
cat <<END_HAVEGED_CONFIG | tee ${cloud_random_config}
random_seed:
    command: ["rc-service", "haveged", "start"]
    command_required: true
END_HAVEGED_CONFIG

cloud_network_config=/etc/cloud/cloud.cfg.d/97-disable-network-config.cfg
echo "[!] Writing cloud network config: ${cloud_network_config}"
cat <<END_CLOUD_CONFIG | tee ${cloud_network_config}
network:
  config: disabled
END_CLOUD_CONFIG

cloud_openstack_config=/etc/cloud/cloud.cfg.d/98-openstack-disable-network-config.cfg
echo "[!] Writing cloud openstack config: ${cloud_openstack_config}"
cat <<END_CLOUD_CONFIG_OPENSTACK | tee "${cloud_openstack_config}"
datasource:
 OpenStack:
  apply_network_config: False
END_CLOUD_CONFIG_OPENSTACK

cloud_growpart_config=/etc/cloud/cloud.cfg.d/99-growpart.cfg
echo "[!] Writing growpart config: ${cloud_growpart_config}"
cat <<END_CLOUD_GROWPART | tee ${cloud_growpart_config}
growpart:
  imode: growpart
  devices:
    - "/dev/vda2"
  ignore_growroot_disabled: true
END_CLOUD_GROWPART

echo "create initial user and unlock account"
adduser -D alpine -G wheel
echo 'Defaults exempt_group=wheel' >/etc/sudoers
sed -i 's/alpine:!/alpine:*/g' /etc/shadow
grep alpine /etc/shadow

# Disable Password auth via SSH
sed -ri '/#?PasswordAuthentication (yes|no)/c\PasswordAuthentication no' /etc/ssh/sshd_config

rc-service syslog stop

# Remove Zeroes
echo "remove files"
/bin/rm -vf /root/.ssh
/bin/rm -vf ~/.ash_history
unset HISTFILE
/bin/rm -rf /var/cache/apk/*
/bin/rm -vf /var/log/dmesg.old

echo "Remove ssh keys"
/bin/rm -vf /etc/ssh/*key*

echo "Zero logs"
>/var/log/messages
>/var/log/dmesg

echo "remove HWADD"
/bin/sed -ri '/hwaddress/d' /etc/network/interfaces

# Fill Disk with Zeros
echo "Zero disk with dd"
dd if=/dev/zero of=/zero.out status=progress || true
rm -vf /zero.out

# I saw someone do this, and let's face it, who isn't a solaris junkie...
# I think it's probably unnecessary but I actually thought there might be some value...
# it doesn't hurt anything to at the risk of over explaining cargocultisim...
sync
sync

exit 0
