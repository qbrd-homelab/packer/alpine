#!/usr/bin/env bash

set -x

echo $1
packer_template_name=${packer_template_name:-$1}
packer_template_name=${packer_template_name:-alpine.json}

. "${CI_PROJECT_DIR:-$(pwd)}/run/variables.sh"
PACKER_LOG=${PACKER_lOG:-1} \
    packer build -var serial="${serial:-$(tty)}" \
    -var-file="${CI_PROJECT_DIR}/run/variables.json" \
    "$1"
