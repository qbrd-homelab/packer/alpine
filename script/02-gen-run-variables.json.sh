#!/usr/bin/env bash

set -u

my_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"
. ${my_dir}/variables.sh

mkdir -vp "${output_dir}"

# This wholly unmaintainable mess was a joke...
ruby -e '$><<%Q({\n#{$<.readlines.map{ |x| %Q(  \"#{x.chomp!.split(/=/,2).join(%(\":))})}.join(",\n")}\n}\n)' run/variables.sh >run/variables.json

exit 0
jq \
  --arg dir "${CI_PROJECT_DIR}" \
  --arg iso_url "${iso_url}" \
  --arg output_dir "${output_dir}" \
  --arg shasum "${iso_checksum}" \
  --arg timestamp "${timestamp}" \
  --arg vm_name "${vm_name}" \
  '. + { 
    "iso_url": $iso_url, 
    "iso_checksum": $shasum, 
    "output_dir": 
    "root_dir": $dir, 
    "timestamp": $timestamp 
    "vm_name": $vm_name, 
  }' \
  <"${variables}" | tee "${output_variables}"
