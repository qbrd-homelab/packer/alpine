#!/usr/bin/env bash

set -u

my_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"
. ${my_dir}/variables.sh
. ${output_dir}/variables.sh

cat <<EO_ANSWERS >"${output_dir}/answers"
  # Use US layout with US variant
  KEYMAPOPTS="us us"

  # Set hostname
  HOSTNAMEOPTS="-n ${vm_name}"

  # Contents of /etc/network/interfaces
  INTERFACESOPTS="auto lo
iface lo inet loopback

auto eth0
iface eth0 inet dhcp
    hostname ${vm_name}
  "

  # Search domain of example.com, Google public nameserver
  DNSOPTS="-d ${domain} ${dns_server}"

  # Set timezone to UTC
  TIMEZONEOPTS="-z America/Denver"

  # set http/ftp proxy
  PROXYOPTS="none"

  # Add a random mirror
  APKREPOSOPTS="-1"

  # Install Openssh
  SSHDOPTS="-c openssh"

  # Use openntpd
  NTPOPTS="-c chrony"

  # Use /dev/sda as a data disk
  DISKOPTS="-L -m sys /dev/vda"

  # Setup in /media/sdb1
  LBUOPTS="/media/sdb1"
  APKCACHEOPTS="/media/sdb1/cache"
EO_ANSWERS
