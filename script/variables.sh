#!/usr/bin/env bash
set -u

# Get some information about where we're running, shold use GitLab-CI variables here
dir="$(dirname "$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)")"

# Since we aim to run in GitLab, use CI_PROJECT_DIR here
CI_PROJECT_DIR=${CI_PROJECT_DIR:-$dir}

variables=${variables:-"${CI_PROJECT_DIR}/variables.json"}
output_dir=${outout_dir:-"${CI_PROJECT_DIR}/run"}
output_variables=${output_variables:-"${output_dir}/variables.json"}

eval "$(jq -r "to_entries|map(\"\(.key)=\(.value|tostring)\")|.[]" "${variables}")"
if [ -f "${output_variables}" ]; then
  eval "$(jq -r "to_entries|map(\"\(.key)=\(.value|tostring)\")|.[]" "${output_variables}")"
fi

timestamp=${timestamp:-$(date '+%Y%m%d%H%M%S')}

# Build variables based on our inputs
iso_name="alpine-${alpine_type}-${alpine_version}.${alpine_patch}-${alpine_arch}.${alpine_dl_type}"
iso_url="http://dl-cdn.alpinelinux.org/alpine/v${alpine_version}/releases/${alpine_arch}/${iso_name}"
shasum_url="${iso_url}.sha256"

# Build our vm_name if unset
if [ -z ${vm_name+vm_name_set} ]; then
  vm_name="alpine-${alpine_type}-${alpine_version}-${alpine_patch}-${alpine_arch}-${timestamp}"
  # replace any . with -
  vm_name=${vm_name//./-}
fi

# if our shasum isn't calculated, download the shasum file and calculate it.
if [ -z ${iso_checksum+iso_checksum_set} ]; then
  mkdir -p "${output_dir}"
  curl --location --output "${CI_PROJECT_DIR}/run/${iso_name}.sha256" "${shasum_url}"
  shasum_file=($(<"${CI_PROJECT_DIR}/run/${iso_name}.sha256"))
  iso_checksum=${shasum_file[0]}
fi

# SSH Key setup because out password doesn't work for some reason...
ssh_key_file=${ssh_key_file:-"${output_dir}/id_rsa"}

# create key if it doesn't exist
if [[ ! -f "${ssh_key_file}" ]]; then
  ssh-keygen -t rsa -N '' -f "${ssh_key_file}"
fi

public_key_content=$(ssh-keygen -y -f "${ssh_key_file}")
ssh_public_key=${ssh_public_key:-"${public_key_content}"}
