#!/usr/bin/env bash
set -x

. ./run/variables.sh
openstack image create \
  --disk-format "${vm_disk_format:-raw}" \
  --container-format "${vm_container_format:-bare}" \
  --tag alpine \
  --public \
  --file "./output-qemu/${vm_name}" "${vm_name}"
